---
layout: default
title: "Indexboi"
---

# Welcome

## Homepage

Homelab homepage at home  
helpful stackoverflow explanation [right here](https://stackoverflow.com/questions/57419996/jekyll-without-a-theme#57424621)

## Networks
Description of how network addresses are split

### Servers

| Address Ranges  | Description                                                                             |
|-----------------|-----------------------------------------------------------------------------------------|
| 10.0.50.1-10    | keepalived VIPs of pxe server, traefik and DNS (pihole, adguardhome, coredns, lancache) |
| 10.0.50.11-39   | actual multiple instances of the above                                                  |
| 10.0.50.40-99   | kubernetes nodes                                                                        |
| 10.0.50.100-200 | virtualization hosts                                                                    |

### Storage

| Address Ranges  | Description                                                                             |
|-----------------|-----------------------------------------------------------------------------------------|
| 10.0.40.1-99    | simple single-host storage stuff                                                        |
| 10.0.40.100-200 | clustered storage stuff                                                                 |

### Monitoring

| Address Ranges  | Description                                                                             |
|-----------------|-----------------------------------------------------------------------------------------|
| 10.0.30.1-19    | keepalived VIPs of grafana, prometheus, influxdb, telegraf, loki, observium, zabbix     |
| 10.0.30.20-49   | actual multiple instances of the above                                                  |
