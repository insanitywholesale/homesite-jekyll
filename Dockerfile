# no official hugo image so this is necessary
FROM ubuntu as buildsite
RUN apt update && apt -y full-upgrade && apt -y install jekyll
WORKDIR /homesite-jekyll
COPY . .
RUN jekyll b

# good old nginx to serve the files
FROM nginx:alpine
COPY default.conf /etc/nginx/conf.d/
COPY --from=buildsite /homesite-jekyll/_site /usr/share/nginx/html

# vim: set ft=dockerfile:
